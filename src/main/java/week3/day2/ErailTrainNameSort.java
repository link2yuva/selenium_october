package week3.day2;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailTrainNameSort {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();

		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);

		WebElement checkbox = driver.findElementById("chkSelectDateOnly");

		if(checkbox.isSelected())
		{
			checkbox.click();
		}

		driver.findElementByXPath("//a[text()='Train Name']").click();

		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		for (int i = 0; i < rows.size(); i++) {
			WebElement eachRow = rows.get(i);
			List<WebElement> cells = eachRow.findElements(By.tagName("td"));
			for (int j = 0; j < cells.size(); j++) {
				System.out.println(cells.get(j).getText());
			}
		}
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img.png");
		FileUtils.copyFile(src, desc);

		
		
	}

}
