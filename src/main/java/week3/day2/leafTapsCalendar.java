package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.collect.Table.Cell;

public class leafTapsCalendar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_birthDate-button").click();

		WebElement table = driver.findElementByXPath("//div[@class='calendar']/table");
		List<WebElement> tableRows = table.findElements(By.tagName("tr"));
		System.out.println(tableRows.size());
		for(int i=0; i<tableRows.size(); i++)
		{
			WebElement eachRow = tableRows.get(i);
			List<WebElement> Cells = eachRow.findElements(By.tagName("td"));
			//System.out.println(Cells.size());
			
			for(int j=0; j<Cells.size(); j++)
			{
				String value =Cells.get(j).getText();
				/*if(value.equals("1"))
				{
				Cells.get(j).click();
				}*/
				
				System.out.println(value);
			}
		
		}


	}

}
