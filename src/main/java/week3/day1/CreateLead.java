package week3.day1;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");			
		WebElement eleUserName = driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();

		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select se = new Select(source);
		se.selectByVisibleText("Employee");

		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select mc = new Select(market);
		mc.selectByValue("CATRQ_CARNDRIVER");

		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select ind = new Select(industry);
		ind.selectByIndex(3);	

		List<WebElement> options = ind.getAllSelectedOptions();
		
		for (WebElement webElement : options) {
			System.out.println("getAllSelectedOptions:" + webElement.getText());
		}
		
		for (WebElement eachElement : options) {
			if(eachElement.getText().startsWith("C"))
			{
				System.out.println(eachElement.getText());
			}

		}
	}

	/*driver.findElementById("createLeadForm_companyName").sendKeys("Infosys");
		driver.findElementById("createLeadForm_firstName").sendKeys("Yuvaraj");
		driver.findElementById("createLeadForm_lastName").sendKeys("Dhanasekaran");
		driver.findElementByClassName("smallSubmit").click();*/
	//driver.close();


}
