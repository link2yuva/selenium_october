package testCases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ZoomCar {

	public static void main(String args[]) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.zoomcar.com/chennai/");
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='items'])[3]").click();
		driver.findElementByXPath("//button[@class='proceed']").click();
		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date);
		System.out.println("Today's date:"+ today );
		int tomorrow=Integer.parseInt(today)+1;
		System.out.println("Tomorrow's date:"+tomorrow);
		
		//driver.findElementByXPath("((//div[@class='t'])//div)[1]").click();
		
		driver.findElementByXPath("(//div[contains(text(),'"+tomorrow+"')])[1]").click();
		String text =driver.findElementByXPath("//div[@class='day picked ']").getText();
		System.out.println("Day Picked:"+text);
		//Integer dt = Integer.parseInt(text);
		
		driver.findElementByXPath("//button[@class='proceed']").click();
		
		
		/*if(text.contains(String.))
		{
			System.out.println("Matched");
		}*/
		
		/*if(Integer.parseInt(text)==tomorrow)
		{
			System.out.println("Matched");
		}*/
		driver.findElementByXPath("//button[@class='proceed']").click();
		List<WebElement> CarList = driver.findElementsByClassName("car-listing");
		System.out.println(CarList.size());

		/*for (WebElement webElement : CarList) {
			System.out.println(webElement.getText());
		}*/
		
		
		List<Integer> lsPrice = new ArrayList<>();

		//Map<Integer, String> carPrice = new TreeMap<>();		
		for (int i = 1; i < CarList.size(); i++) {
			//String cName=driver.findElementByXPath("((//div[@class='details'])/following::h3)[" + i +"]").getText();
			String cAmt = driver.findElementByXPath("(//div[@class='price'])["+i+"]").getText();
			
			/*System.out.println(cName);
			System.out.println(cAmt);*/
			//String cAmt = (driver.findElementByXPath("(//sup[@class='rupee'])["+ i+ "]").getText());
			cAmt = cAmt.replaceAll("₹ ","");
			Integer cAmount = Integer.parseInt(cAmt);
			lsPrice.add(cAmount);
		}
		Collections.sort(lsPrice);

		System.out.println(lsPrice.get(lsPrice.size()-1));
		
		int HighCost = lsPrice.get(lsPrice.size()-1);
		
		driver.findElementByXPath("//div[contains(text(),'"+HighCost+"')]/following::button").click();
		
		driver.close();
		
		/*String text = driver.findElementByXPath("((//div[@class='day'])//div)[1]").getText();
		System.out.println(text);
		if()*/
		
		//driver.findElementByXPath(using)

	}
}
