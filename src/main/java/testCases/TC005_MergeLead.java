package testCases;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC005_MergeLead extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testName ="TC005_MergeLead";
		testDesc ="Merge existing lead in leaftaps";
		author ="Yuvaraj";
		category = "Smoke";
		fileName="mergelead";
	}

	@Test(dataProvider="fetchData", enabled=true)
	public void MergeLead(String fromId, String toId, String exactText) {
		// TODO Auto-generated method stub
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Merge Leads"));
		click(locateElement("xpath", "(//table[@id='widget_ComboBox_partyIdFrom']//following::img)[1]"));
		switchToWindow(1);
		type(locateElement("xpath", "//input[@type='text' and @name='id']"),fromId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "(//a[@class='linktext'])[1]")));
		clickWithoutNoSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		switchToWindow(0);
		click(locateElement("xpath", "(//table[@id='widget_ComboBox_partyIdTo']//following::img)[1]"));
		switchToWindow(1);
		type(locateElement("xpath", "//input[@type='text' and @name='id']"), toId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "(//a[@class='linktext'])[1]")));
		clickWithoutNoSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		switchToWindow(0);
		clickWithoutNoSnap(locateElement("class", "buttonDangerous"));
		acceptAlert();
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		type(locateElement("xpath", "//input[@type='text' and @name='id']"), fromId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "//div[@class='x-paging-info']")));
		verifyExactText(locateElement("xpath", "//div[@class='x-paging-info']"), exactText);
	}

}
