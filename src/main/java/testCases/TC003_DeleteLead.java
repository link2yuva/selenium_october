package testCases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC003_DeleteLead extends ProjectMethods{

	@BeforeClass(groups="config")
	public void setData() {
		testName ="TC003_DeleteLead";
		testDesc ="Delete lead in leaftaps";
		author ="Yuvaraj";
		category = "Smoke";
		fileName="deletelead";
	} 

	@Test(dataProvider="fetchData")
	public void DeleteLead(String phNumber, String exactText) {
		// TODO Auto-generated method stub
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("xpath", "//span[text()='Phone']"));
		type(locateElement("xpath", "//input[@name='phoneNumber']"), phNumber);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "(//a[@class='linktext'])[4]")));
		String text = getText(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		click(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		click(locateElement("class", "subMenuButtonDangerous"));
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		type(locateElement("xpath", "//input[@type='text' and @name='id']"), text);
		click(locateElement("xpath", "//button[text()='Find Leads']"));		
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "//div[@class='x-paging-info']")));
		verifyExactText(locateElement("xpath", "//div[@class='x-paging-info']"), exactText);	
	}

}
