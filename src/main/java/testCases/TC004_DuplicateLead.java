package testCases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC004_DuplicateLead extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testName ="TC004_DuplicateLead";
		testDesc ="Create Duplicate lead in leaftaps";
		author ="Yuvaraj";
		category = "Smoke";
		fileName="duplicatelead";
	}
	
	@Test(dataProvider="fetchData")
	public void DuplicateLead(String eMailId) {
		// TODO Auto-generated method stub
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("xpath", "//span[text()='Email']"));
		type(locateElement("xpath", "//input[@name='emailAddress']"), eMailId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "(//a[@class='linktext'])[6]")));
		String text = getText(locateElement("xpath", "(//a[@class='linktext'])[6]"));
		click(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		click(locateElement("xpath", "//a[@class='subMenuButton' and text()='Duplicate Lead']"));
		verifyTitle("Duplicate Lead | opentaps CRM");
		click(locateElement("xpath", "//input[@name='submitButton']"));
		verifyExactText(locateElement("viewLead_firstName_sp"), text);
	}

}
