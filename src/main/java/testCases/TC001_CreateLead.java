package testCases;

import java.io.IOException;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import util.ReadExcel;
import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeClass(groups="config")
	public void setData() {
		testName ="TC001_CreateLead";
		testDesc ="Create a new lead in leaftaps";
		author ="Yuvaraj";
		category = "Smoke";
		fileName="createlead";
	} 

	@Test(dataProvider="fetchData")
	public void Createlead(String cName, String fName, String lName, String source, String marketingMampaign, String fNameLocal,
			String lNameLocal, String salutation, String title, String department, String annualRevenue, String preferredCurrency,
			String industry, String NoOfEmployees, String Ownership, String sicCode, String ticker, String description, String impNote,
			String countryCode, String areaCode, String phNumber, String extnNum, String askForName, String eMailId, String webUrl,
			String toName, String AttnName, String address1, String address2, String city, String country, String state, String pinCode, 
			String pinCodeExt)
	{
		click(locateElement("link","Create Lead"));
		String parentWindow = getParentWindow();
		type(locateElement("id", "createLeadForm_companyName"), cName);
		click(locateElement("xpath", "(//input[@id='createLeadForm_parentPartyId']//following::img)[1]"));
		switchToWindow(1);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "(//a[@class='linktext'])[1]")));
		clickWithoutNoSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		switchToParentWindow(parentWindow);
		type(locateElement("id", "createLeadForm_firstName"), fName);
		type(locateElement("id", "createLeadForm_lastName"), lName);
		selectDropDownUsingText(locateElement("id", "createLeadForm_dataSourceId"), source);
		selectDropDownUsingValue(locateElement("id", "createLeadForm_marketingCampaignId"), marketingMampaign);
		type(locateElement("id", "createLeadForm_firstNameLocal"), fNameLocal);
		type(locateElement("id","createLeadForm_lastNameLocal"), lNameLocal);
		type(locateElement("id", "createLeadForm_personalTitle"), salutation);
		type(locateElement("id", "createLeadForm_generalProfTitle"), title);
		type(locateElement("id", "createLeadForm_departmentName"), department);
		type(locateElement("id", "createLeadForm_annualRevenue"), annualRevenue);
		selectDropDownUsingText(locateElement("id", "createLeadForm_currencyUomId"), preferredCurrency);
		selectDropDownUsingText(locateElement("id", "createLeadForm_industryEnumId"), industry);
		type(locateElement("createLeadForm_numberEmployees"), NoOfEmployees);
		selectDropDownUsingText(locateElement("createLeadForm_ownershipEnumId"), Ownership);
		type(locateElement("createLeadForm_sicCode"), sicCode);
		type(locateElement("createLeadForm_tickerSymbol"), ticker);
		type(locateElement("createLeadForm_description"), description);
		type(locateElement("createLeadForm_importantNote"), impNote);
		clearAndType(locateElement("createLeadForm_primaryPhoneCountryCode"), countryCode);
		type(locateElement("createLeadForm_primaryPhoneAreaCode"), areaCode);
		type(locateElement("createLeadForm_primaryPhoneNumber"), phNumber);
		type(locateElement("createLeadForm_primaryPhoneExtension"), extnNum);
		type(locateElement("createLeadForm_primaryPhoneAskForName"), askForName);
		type(locateElement("createLeadForm_primaryEmail"), eMailId);
		type(locateElement("createLeadForm_primaryWebUrl"), webUrl);
		type(locateElement("createLeadForm_generalToName"), toName);
		type(locateElement("createLeadForm_generalAttnName"), AttnName);
		type(locateElement("createLeadForm_generalAddress1"), address1);
		type(locateElement("createLeadForm_generalAddress2"), address2);
		type(locateElement("createLeadForm_generalCity"), city);
		selectDropDownUsingText(locateElement("createLeadForm_generalCountryGeoId"), country);
		selectDropDownUsingText(locateElement("createLeadForm_generalStateProvinceGeoId"), state);
		type(locateElement("createLeadForm_generalPostalCode"), pinCode);
		type(locateElement("createLeadForm_generalPostalCodeExt"), pinCodeExt);
		click(locateElement("class", "smallSubmit"));
		verifyTitle("View Lead | opentaps CRM");
		getText(locateElement("viewLead_companyName_sp"));
	}

}
