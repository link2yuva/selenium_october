package testCases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;
import wdmethods.SeMethods;

public class TC002_EditLead extends ProjectMethods{

	@BeforeClass(groups="config")
	public void setData() {
		testName ="TC002_EditLead";
		testDesc ="Edit lead in leaftaps";
		author ="Yuvaraj";
		category = "Smoke";
		fileName="editlead";
	} 

	@Test(dataProvider="fetchData")
	public void EditLead(String fName, String cName, String verifyText)
	{
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[last()]"), fName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "(//a[@class='linktext'])[4]")));
		click(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		verifyTitle("View Lead | opentaps CRM");
		click(locateElement("xpath", "//a[@class='subMenuButton' and text()='Edit']"));
		clearAndType(locateElement("updateLeadForm_companyName"), cName);
		click(locateElement("xpath", "//input[@value='Update']"));
		verifyPartialText(locateElement("viewLead_companyName_sp"), verifyText);
	}

}
