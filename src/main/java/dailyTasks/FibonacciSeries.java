package dailyTasks;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter any Number to print its Fibonacci Series:");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int sum=0, n1=0, n2=1;
		while(n1<=n)
		{
			System.out.println(n1);
			sum=n1+n2;
			n1=n2;
			n2=sum;
		}
		sc.close();
		
	}

}
