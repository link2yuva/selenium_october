package dailyTasks;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter any word:");
		String str = sc.next();
		String buffer = new StringBuffer(str).reverse().toString();
		//StringBuffer
		//System.out.println(buffer.reverse());
		
		//String Text = buffer.reverse().toString();
		
		if(str.equals(buffer))
			System.out.println("Palindrome");
		else
			System.out.println("Not Palindrome");
	}

}
