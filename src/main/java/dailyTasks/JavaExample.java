package dailyTasks;

class JavaExample{
	/*static int num;
	   static String mystr;
	   static{
	      num = 97;
	      mystr = "Static keyword in Java";
	   }
	   public static void main(String args[])
	   {
	      System.out.println("Value of num: "+num);
	      System.out.println("Value of mystr: "+mystr);
	   }*/

	/*static int num;
	static String mystr;
	//First Static block
	static{
		System.out.println("Static Block 1");
		num = 68;
		mystr = "Block1";
	} 
	//Second static block
	static{
		System.out.println("Static Block 2");
		num = 98;
		mystr = "Block2";
	}
	public static void main(String args[])
	{
		System.out.println("Value of num: "+num);
		System.out.println("Value of mystr: "+mystr);
	}*/

	/*static int var1;
	static String var2;
	//This is a Static Method
	static void disp(){
		System.out.println("Var1 is: "+var1);
		System.out.println("Var2 is: "+var2);
	}
	public static void main(String args[]) 
	{
		disp();
	}*/


	//************************************************************

	/*//Static integer variable
	   static int var1=77; 
	   //non-static string variable
	   String var2;

	   public static void main(String args[])
	   {
		JavaExample ob1 = new JavaExample();
		JavaExample ob2 = new JavaExample();
		 static variables can be accessed directly without
	 * any instances. Just to demonstrate that static variables
	 * are shared, I am accessing them using objects so that 
	 * we can check that the changes made to static variables
	 * by one object, reflects when we access them using other
	 * objects

	        //Assigning the value to static variable using object ob1
		ob1.var1=88;
		ob1.var2="I'm Object1";
	         This will overwrite the value of var1 because var1 has a single 
	 * copy shared among both the objects.

	        ob2.var1=99;
		ob2.var2="I'm Object2";
		System.out.println("ob1 integer:"+ob1.var1);
		System.out.println("ob1 String:"+ob1.var2);
		System.out.println("ob2 integer:"+ob2.var1);
		System.out.println("ob2 STring:"+ob2.var2);
	   }*/

	// ******************************

	/* static int i = 10;
	   static String s = "Beginnersbook";
	   //This is a static method
	   public static void main(String args[]) 
	   {
	       System.out.println("i:"+i);
	       System.out.println("s:"+s);
	   }*/

	//*******************************

/*	static int i = 100;
	static String s = "Beginnersbook";
	//Static method
	static void display()
	{
		System.out.println("i:"+i);
		System.out.println("i:"+s);
	}

	//non-static method
	void funcn()
	{
		//Static method called in non-static method
		display();
	}
	//static method
	public static void main(String args[])
	{
		JavaExample obj = new JavaExample();
		//You need to have object to call this non-static method
		obj.funcn();

		//Static method called in another static method
		display();
	}*/



}


