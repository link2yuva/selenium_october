package dailyTasks;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Enter any number to reverse:");
		Scanner sc = new Scanner(System.in);
		Integer n = sc.nextInt();
		// System.out.println(n);
		String text = n.toString();
		char[] A = text.toCharArray();

		/*for(int i=A.length-1; i>=0; i--)	
		{
			System.out.print(A[i]);
		}*/
		
		int q=n, reminder, result=0;
		while(q>0)
		{
			reminder = q%10;
			q = q/10;	
			result=result*10;
			result = result+reminder;
		}
		System.out.println(result);
		sc.close();

	}


}
