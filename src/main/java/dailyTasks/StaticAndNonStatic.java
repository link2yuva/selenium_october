package dailyTasks;

public class StaticAndNonStatic {

	static String text1="Hello";
	static String text2="World";

	public static void method1()
	{
		System.out.println(text1+" "+text2);
	}

	public void method2()
	{
		System.out.println(text1+" "+text2);
	}
	
	public static void main(String args[]) {
		
		method1();
		StaticAndNonStatic mtd2 = new StaticAndNonStatic();
		mtd2.method2();;
		
	}

}
