package dailyTasks;

class SimpleStaticExample {

	/*static void myMethod()
    {
        System.out.println("myMethod");
    }

    public static void main(String[] args)
    {
           You can see that we are calling this
	 * method without creating any object. 

           myMethod();
    }*/

	/*private static String str = "BeginnersBook";

	//Static class
	static class MyNestedClass{
		//non-static method
		public void disp() {

			 If you make the str variable of outer class
	 * non-static then you will get compilation error
	 * because: a nested static class cannot access non-
	 * static members of the outer class.

			System.out.println(str); 
		}

	}
	public static void main(String args[])
	{
		 To create instance of nested class we didn't need the outer
	 * class instance but for a regular nested class you would need 
	 * to create an instance of outer class first

		SimpleStaticExample.MyNestedClass obj = new SimpleStaticExample.MyNestedClass();
		obj.disp();
	}
	 */

	public static void main(String args[])
	{

		/*	for (int j=0; j<=6; j++)
	{
		if (j==4)
		{
			continue;
		}

		System.out.print(j+" ");
		 */ //}

		int num =0;
		while(num<=100)
		{
			System.out.println("Value of variable is: "+num);
			if (num==2)
			{
				break;
			}
			num++;
		}
		System.out.println("Out of while-loop");
	}

}
