package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ListDuplicateEmpId {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Integer> empid = new ArrayList<>();
		empid.add(100);
		empid.add(101);
		empid.add(102);
		empid.add(103);
		empid.add(100);
		empid.add(101);
		empid.add(102);

		Collections.sort(empid);

		System.out.println("Before removing duplicates:");
		
		for (int i : empid) {
			System.out.println(i);
		}		

		Set<Integer> Empid = new TreeSet<>();

		Empid.addAll(empid);
		
		System.out.println("After Removing duplicates:");

		for (int i : Empid) {
			System.out.println(i);
		}
	}

}
