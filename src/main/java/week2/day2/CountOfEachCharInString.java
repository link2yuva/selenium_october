package week2.day2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class CountOfEachCharInString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String text = "Yuvaraj Dhanasekaran";
		char[] A = text.toCharArray();
		
		Map<Character, Integer> CharCount = new LinkedHashMap<>();

		for (char c : A) {
			if (CharCount.containsKey(c)) {
				int value = CharCount.get(c);
				CharCount.put(c, value+1);
			} else {
               CharCount.put(c, 1);
			}
			
		}
		System.out.println(CharCount);		
	}
}