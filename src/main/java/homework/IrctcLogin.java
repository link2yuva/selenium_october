package homework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IrctcLogin {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		driver.findElementByLinkText("Check Availability").click();
		
		System.out.println(driver.switchTo().alert().getText());
		
		driver.switchTo().alert().accept();

		driver.findElementById("userRegistrationForm:userName").sendKeys("link2yuva");

		driver.findElementById("userRegistrationForm:password").sendKeys("jasmines");

		driver.findElementById("userRegistrationForm:confpasword").sendKeys("jasmines");

		WebElement SecurityQuest = driver.findElementById("userRegistrationForm:securityQ");
		Select SectQuest = new Select(SecurityQuest);
		SectQuest.selectByIndex(2);
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Testing");
		
		WebElement PreLanguage = driver.findElementById("userRegistrationForm:prelan");
		Select PreLang = new Select(PreLanguage);
		PreLang.selectByIndex(0);
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Yuvaraj");
		
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Middle");
		
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Dhanasekaran");
		
		driver.findElementById("userRegistrationForm:gender:0").click();
		
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		
		WebElement BirthDay = driver.findElementById("userRegistrationForm:dobDay");
		Select day = new Select(BirthDay);
		day.selectByValue("15");
		
		WebElement BirthMonth = driver.findElementById("userRegistrationForm:dobMonth");
		Select month = new Select(BirthMonth);
		month.selectByIndex(7);
		
		WebElement BirthYear = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select year = new Select(BirthYear);
		year.selectByVisibleText("1986");
		
		WebElement Occupation = driver.findElementById("userRegistrationForm:occupation");
		Select Occup = new Select(Occupation);
		Occup.selectByVisibleText("Private");
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("9876543210");
		
		driver.findElementById("userRegistrationForm:idno").sendKeys("ACGPY6445B");
		
		WebElement Country = driver.findElementById("userRegistrationForm:countries");
		Select country = new Select(Country);
		country.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("link2yuva@gmail.com");
		
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9176690306");
		
		WebElement Nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select nation = new Select(Nationality);
		nation.selectByIndex(1);
		
		driver.findElementById("userRegistrationForm:address").sendKeys("Selvam Apartments");
		
		driver.findElementById("userRegistrationForm:street").sendKeys("North Park Street");
		
		driver.findElementById("userRegistrationForm:area").sendKeys("Ambattur");
		
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600053",Keys.TAB);
		
		Thread.sleep(3000);
		
		WebElement City = driver.findElementById("userRegistrationForm:cityName");
		Select city = new Select(City);
		city.selectByIndex(1);
		
		Thread.sleep(3000);

		
		WebElement postoffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select postoff= new Select(postoffice);
		
		/*WebDriverWait wait  = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeSelected(postoffice));*/
		
		postoff.selectByIndex(1);
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("6543210");
		
		
	}

}
