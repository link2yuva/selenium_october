package homework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("http://leaftaps.com/opentaps");	

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		WebElement eleUserName = driver.findElementById("username");

		eleUserName.sendKeys("DemoSalesManager");

		driver.findElementById("password").sendKeys("crmsfa");

		driver.findElementByClassName("decorativeSubmit").click();

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Find Leads").click();

		driver.findElementByXPath("//span[text()='Email']").click();

		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("link2yuva@gmail.com");

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(3000);

		String FName = driver.findElementByXPath("(//a[@class='linktext'])[6]").getText();

		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();

		driver.findElementByXPath("//a[@class='subMenuButton' and text()='Duplicate Lead']").click();

		String pageTitle = driver.getTitle();

		if(pageTitle.equals("Duplicate Lead | opentaps CRM"))
		{
			System.out.println("Page Name Matching"); 
		}
		else
			System.out.println("Page Name Not Matching");
		
		driver.findElementByXPath("//input[@name='submitButton']").click();
		
		String NewFName = driver.findElementById("viewLead_firstName_sp").getText();
		
		if(FName.equals(NewFName))
		{
			System.out.println("Confirm the duplicated lead name is same as captured name");
		}
		else
			System.out.println("Different Name");

		
		
	}


}
