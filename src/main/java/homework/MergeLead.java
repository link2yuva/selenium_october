package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("http://leaftaps.com/opentaps");	

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		WebElement eleUserName = driver.findElementById("username");

		eleUserName.sendKeys("DemoSalesManager");

		driver.findElementById("password").sendKeys("crmsfa");

		driver.findElementByClassName("decorativeSubmit").click();

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Merge Leads").click();

		String parentWindow = driver.getWindowHandle();

		driver.findElementByXPath("(//table[@id='widget_ComboBox_partyIdFrom']//following::img)[1]").click();

		Set<String> Frmwindows = driver.getWindowHandles();
		List<String> allWindows = new ArrayList<>();
		allWindows.addAll(Frmwindows);
		driver.switchTo().window(allWindows.get(allWindows.size()-1));

		driver.findElementByXPath("//input[@type='text' and @name='id']").sendKeys("10490");

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(3000);

		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();

		driver.switchTo().window(parentWindow);

		driver.findElementByXPath("(//table[@id='widget_ComboBox_partyIdTo']//following::img)[1]").click();

		Set<String> Towindows = driver.getWindowHandles();
		List<String> allWindowsTo = new ArrayList<>();
		allWindowsTo.addAll(Towindows);
		driver.switchTo().window(allWindowsTo.get(allWindowsTo.size()-1));

		driver.findElementByXPath("//input[@type='text' and @name='id']").sendKeys("10491");

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(3000);

		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();

		driver.switchTo().window(parentWindow);

		driver.findElementByClassName("buttonDangerous").click();

		driver.switchTo().alert().accept();

		Thread.sleep(3000);

		driver.findElementByXPath("//a[text()='Find Leads']").click();

		driver.findElementByXPath("//input[@type='text' and @name='id']").sendKeys("10490");

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(3000);

		String ErrorMsg = driver.findElementByXPath("//div[@class='x-paging-info']").getText();

		if(ErrorMsg.equals("No records to display"))
		{
			System.out.println("No records to display");
		}
		else
			System.out.println("Id present in table");

		driver.close();

	}

}
