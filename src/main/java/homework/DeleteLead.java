package homework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DeleteLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("http://leaftaps.com/opentaps");	

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		WebElement eleUserName = driver.findElementById("username");

		eleUserName.sendKeys("DemoSalesManager");

		driver.findElementById("password").sendKeys("crmsfa");

		driver.findElementByClassName("decorativeSubmit").click();

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Find Leads").click();
		
		driver.findElementByXPath("//span[text()='Phone']").click();
		
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9176690306");
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);
		
		String leadID = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		
		System.out.println(leadID);
		
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		
		driver.findElementByClassName("subMenuButtonDangerous").click();
		
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		
		driver.findElementByXPath("//input[@type='text' and @name='id']").sendKeys(leadID);
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(5000);

		String ErrorMsg = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		
		if(ErrorMsg.equals("No records to display"))
		{
			System.out.println("No records to display");
		}
		else
			System.out.println("Id present in table");
		
		driver.close();
	}

}
