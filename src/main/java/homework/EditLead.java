package homework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("http://leaftaps.com/opentaps");	

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		WebElement eleUserName = driver.findElementById("username");

		eleUserName.sendKeys("DemoSalesManager");

		driver.findElementById("password").sendKeys("crmsfa");

		driver.findElementByClassName("decorativeSubmit").click();

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Find Leads").click();

		driver.findElementByXPath("(//input[@name='firstName'])[last()]").sendKeys("Yuvaraj");

		driver.findElementByXPath("//button[text()='Find Leads']").click();

		Thread.sleep(3000);

		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();

		String pageTitle = driver.getTitle();

		if(pageTitle.equals("View Lead | opentaps CRM"))
		{
			System.out.println("Page Name Matching"); 
		}
		else
			System.out.println("Page Name Not Matching");

		driver.findElementByXPath("//a[@class='subMenuButton' and text()='Edit']").click();
		
		driver.findElementById("updateLeadForm_companyName").clear();

		driver.findElementById("updateLeadForm_companyName").sendKeys("Testing");

		driver.findElementByXPath("//input[@value='Update']").click();

		String companyName = driver.findElementById("viewLead_companyName_sp").getText();

		if(companyName.contains("Testing"))
		{
			System.out.println("Company Name Matching");
		}
		else
			System.out.println("Company Name Not Matching");

		driver.close();
	}

}
