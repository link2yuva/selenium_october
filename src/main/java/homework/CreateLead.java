package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		/*System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		FirefoxDriver driver=new FirefoxDriver();*/

		/*System.setProperty("webdriver.ie.driver", "./drivers/MicrosoftWebDriver.exe");
		InternetExplorerDriver driver = new InternetExplorerDriver();*/

		driver.manage().window().maximize();

		driver.get("http://leaftaps.com/opentaps");	

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		WebElement eleUserName = driver.findElementById("username");

		eleUserName.sendKeys("DemoSalesManager");

		driver.findElementById("password").sendKeys("crmsfa");

		driver.findElementByClassName("decorativeSubmit").click();
		
		Thread.sleep(3000);

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Create Lead").click();

		driver.findElementById("createLeadForm_companyName").sendKeys("Infosys");
		
		String parentWindow = driver.getWindowHandle();
		
		driver.findElementByXPath("(//input[@id='createLeadForm_parentPartyId']//following::img)[1]").click();
		
		Set<String> allWindows = driver.getWindowHandles();
		List<String> eachWindow = new ArrayList<>();
		eachWindow.addAll(allWindows);
		driver.switchTo().window(eachWindow.get(1));
		
		Thread.sleep(3000);
		
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		
		/*driver.findElementByXPath("//span[text()='Phone']").click();
		
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9176690306");
		
		driver.findElementByXPath("//button[text()='Find Accounts']").click();*/
		
		driver.switchTo().window(parentWindow);

		driver.findElementById("createLeadForm_firstName").sendKeys("Yuvaraj");

		driver.findElementById("createLeadForm_lastName").sendKeys("Dhanasekaran");

		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select sc = new Select(src);
		sc.selectByVisibleText("Conference");

		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select Mkt=new Select(market);
		Mkt.selectByVisibleText("Automobile");

		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Yuva");

		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Dhanasekaran");

		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");
		
		driver.findElementByXPath("//img[@id='createLeadForm_birthDate-button']").click();
		
	/*	WebElement table = driver.findElementById("createLeadForm_birthDate-calendar-placeholder");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		for (int i = 0; i < rows.size(); i++) {
			WebElement eachRow = rows.get(i);
			List<WebElement> cells = eachRow.findElements(By.tagName("td"));
			for (int j = 0; j < cells.size(); j++) {
				System.out.println(cells.get(j).getText());
			}
		}*/
		

		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Title");

		driver.findElementById("createLeadForm_departmentName").sendKeys("IVS");

		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");

		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select cur = new Select(currency);
		cur.selectByVisibleText("DZD - Algerian Dinar");

		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select ind = new Select(industry);
		ind.selectByVisibleText("Computer Hardware");

		driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");

		WebElement Owner = driver.findElementById("createLeadForm_ownershipEnumId");
		Select own = new Select(Owner);
		own.selectByVisibleText("Partnership");

		driver.findElementById("createLeadForm_sicCode").sendKeys("12345");

		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Ticker");

		driver.findElementById("createLeadForm_description").sendKeys("Description");

		driver.findElementById("createLeadForm_importantNote").sendKeys("Notes");

		driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();

		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");

		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");

		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9176690306");

		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("74751");

		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Yuvaraj");

		driver.findElementById("createLeadForm_primaryEmail").sendKeys("link2yuva@gmail.com");

		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("Infosys.com");

		driver.findElementById("createLeadForm_generalToName").sendKeys("Yuvaraj");

		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Dhanasekaran");

		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Address1");

		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Address2");

		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");

		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select State = new Select(state);
		State.selectByVisibleText("Alabama");

		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600053");

		WebElement Country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select country = new Select(Country);
		country.selectByVisibleText("Algeria");

		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("53");

		driver.findElementByClassName("smallSubmit").click();
		
		System.out.println("Lead id created");

		driver.close();
	}

}
