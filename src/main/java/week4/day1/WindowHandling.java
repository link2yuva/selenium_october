package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.Get;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		String parentWindow = driver.getWindowHandle();

		driver.findElementByLinkText("Contact Us").click();

		Set<String> windowHandles = driver.getWindowHandles();
		List<String> allWindows = new ArrayList<>();
		allWindows.addAll(windowHandles);
		/*for (String string : allWindows) {
			System.out.println("All Windwos:" + driver.getUrl());
		}*/

		driver.switchTo().window(allWindows.get(allWindows.size()-1));
		System.out.println();
		System.out.println(driver.getTitle());
		driver.switchTo().window(parentWindow);
		System.out.println();
		if(driver.getCurrentUrl().equals("https://www.irctc.co.in/eticketing/userSignUp.jsf"))
		{
			System.out.println("In parent window");
		}

		/*List<WebElement> Tags = driver.findElementsByTagName("a");
		for (WebElement webElement : Tags) {
			System.out.println(webElement.getText());		
		}*/
		/*System.out.println(driver.getPageSource());
		System.out.println(driver.getSessionId());*/
		driver.quit();

	}

}
