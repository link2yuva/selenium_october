package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertandFrames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");

		WebElement eleFrame = driver.findElementByXPath("//iframe[@id='iframeResult']");

		driver.switchTo().frame(eleFrame);

		driver.findElementByXPath("//button[text()='Try it']").click();

		Alert promptAlert = driver.switchTo().alert();
		promptAlert.sendKeys("Yuvaraj");
		promptAlert.accept();

		String text = driver.findElementByXPath("//p[@id='demo']").getText();
		System.out.println(text);

		if(text.contains("Yuvaraj"))
		{
			System.out.println("Name Present");
		}
		//System.out.println(driver.findElementByXPath("//p[@id='demo']").getLocation());
	}

}
