package util;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static String[][] getExcelData(String fileName) throws IOException {
		XSSFWorkbook myBook = new XSSFWorkbook("./data/"+fileName+".xlsx");
		XSSFSheet sheet = myBook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		int colCount = sheet.getRow(0).getLastCellNum();
		String[][] data = new String[rowCount][colCount];
		for (int i = 1; i <=rowCount; i++) {	
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < colCount; j++) {
				XSSFCell cell = row.getCell(j);
				String CellValue = cell.getStringCellValue();
				data[i-1][j]=CellValue;
				//System.out.println(CellValue);
			} 
		}
		//myBook.close();
		return data;
	}

}
