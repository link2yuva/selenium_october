package util;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static void main(String[] args) throws IOException {
		XSSFWorkbook myBook = new XSSFWorkbook("./data/createlead.xlsx");
		XSSFSheet sheet = myBook.getSheet("createlead");
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		
		for (int i = 1; i <=lastRowNum; i++) {	
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < lastCellNum; j++) {
				XSSFCell cell = row.getCell(j);
				String CellValue = cell.getStringCellValue();
				System.out.println(CellValue);
			} 
		}
		myBook.close();
	}

}
