package util;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporter_Basic {
	
	@Test
	public void reports() throws IOException
	{
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/results.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC001_CreateLead", "Create Lead in Leaftaps");
		test.assignAuthor("Yuvaraj");
		test.assignCategory("Regression");
		test.pass("step 1: passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		test.pass("step 2: passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("step 3: passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("step 4: failed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("step 4: warning", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
		extent.flush();
	}

}
