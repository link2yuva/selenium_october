package util;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporter {

	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public static ExtentTest test;
	public String testName, testDesc, author,  category, fileName;

	@BeforeSuite(groups="config")
	public void startResult()
	{
		html = new ExtentHtmlReporter("./reports/results.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}	

	@BeforeMethod(groups="config")
	public void startTestCase()
	{
		test = extent.createTest(testName, testDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}

	public void reportSteps(String status, String desc)
	{
		if (status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		} else if (status.equalsIgnoreCase("fail")) 
		{
			test.fail(desc);
		}
	}

	@AfterSuite(groups="config")
	public void endResult()
	{
		extent.flush();
	}

}
