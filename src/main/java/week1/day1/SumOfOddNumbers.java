package week1.day1;

import java.util.Scanner;

public class SumOfOddNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter any Number:");
		int A = sc.nextInt();
		int sum=0;
		while(A>0)
		{
			int i=A%10;
			if(i%2==1)
			{
				sum=sum+i;
			}
			A=A/10;
		}
		System.out.println(sum);
		sc.close();
	}

}
