package week1.day1;

import java.util.Scanner;

public class PrimeNumberOrNot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Enter any Number:");
		Scanner sc = new Scanner(System.in);
		int i = sc.nextInt();
		
		if(i==2 || i==3)
		{
			System.out.println("Prime Number");
		}
		else if(!(i%2==0 || i%3==0))
		{
			System.out.println("Prime Number");
		}
		else
			System.out.println("Not Prime Number");
		sc.close();
	}

}
