package week1.day1;

import java.util.Scanner;

public class Greatestof3Numbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Any 3 Number to Find greatest:");
		
		int A= sc.nextInt();
		int B= sc.nextInt();
		int C= sc.nextInt();
		
		if(A>B && A>C)
			System.out.println(A+ " is greatest");
		else if(B>A && B>C)
			System.out.println(B + " is greatest");
		else
			System.out.println(C + " is greatest");	
		sc.close();
	}

}
