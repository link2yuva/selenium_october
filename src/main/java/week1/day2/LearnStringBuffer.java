package week1.day2;

public class LearnStringBuffer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*StringBuffer buffer = new StringBuffer("Yuvaraj");
		System.out.println(buffer);
		System.out.println(buffer.length());*/
		/*System.out.println(buffer.insert(3, 'G'));
		System.out.println(buffer);*/
		// System.out.println(buffer.reverse());
		// System.out.println(buffer.replace(3, 7, "A"));
		
		StringBuilder builder = new StringBuilder("Yuvaraj Dhanasekaran");
		System.out.println(builder.length());
		/*System.out.println(builder.substring(2));
		System.out.println(builder.substring(2, 10));
		System.out.println(builder.reverse());*/
		//System.out.println(builder.insert(5, 'A'));
		
		char[] ch = builder.toString().toCharArray();
		for(char i : ch)
			System.out.print(i);
	}

}
