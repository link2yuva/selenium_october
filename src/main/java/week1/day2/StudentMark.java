package week1.day2;

import java.util.Scanner;

public class StudentMark {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Enter No.of Students:");
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int[] StudentMark = new int[size];
		for(int i=0; i<size; i++)		
			StudentMark[i]=sc.nextInt();		
		for(int a : StudentMark)
			System.out.println(a);		
		int sum=0;
		for(int i=0; i<size; i++)	
		{
			if(StudentMark[i]>=50)
			{
				System.out.println(StudentMark[i] + " Pass");	
				sum++;
			}
			else
				System.out.println(StudentMark[i] + " Fail");
		}
		sc.close();
		System.out.println("No of pass:" + sum);
	}

}
