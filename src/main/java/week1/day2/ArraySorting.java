package week1.day2;

import java.util.Scanner;

public class ArraySorting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter No.of Students:");
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int[] StudentMark = new int[size];
		for(int i=0; i<size; i++)	
		{
			StudentMark[i]=sc.nextInt();
		}

		for(int i=0; i<size; i++)	
		{
			for(int j=1; j<size; j++)
			{
				if(StudentMark[i]>StudentMark[j])
				{
					int a=0;
					a= StudentMark[i];
					StudentMark[i]=StudentMark[j];
					StudentMark[j]=a;
				}
			}
		}
		
		for(int a : StudentMark)
			System.out.println(a);
		sc.close();
	}

}
