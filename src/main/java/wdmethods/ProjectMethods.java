package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import util.ReadExcel;

public class ProjectMethods extends SeMethods{
	@Parameters({"browser", "url", "userName", "password"})
	@BeforeMethod(groups="config")
	public void login(String browser, String url, String userName, String password)
	{
		startApp(browser, url);
		type(locateElement("username"), userName);
		type(locateElement("password"), password);
		click(locateElement("class", "decorativeSubmit"));
		WebElement crmsfalink = locateElement("link", "CRM/SFA");
		click(crmsfalink);
	}
	
	@DataProvider(name="fetchData")
	public String[][] getData() throws IOException
	{		
		return ReadExcel.getExcelData(fileName);
		
	}

	@AfterMethod(groups="config")
	public void logout()
	{
		closeBrowser();
	}

}
