package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.runtime.Timeout;
import util.Reporter;

public class SeMethods extends Reporter implements WdMethods {
	public RemoteWebDriver driver;
	int i =1;
	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			}else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.get(url); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportSteps("pass", "The browser "+browser+" launched successfully");
			//System.out.println("The browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
			reportSteps("fail", "Unknown exception occured while launching the browser");
			//System.err.println("Unknown exception occured while launching the browser");
		} finally {
			takeSnap();
		}
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);	
			case "xpath": return driver.findElementByXPath(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "link": return driver.findElementByLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			reportSteps("fail", "The element is not found");
			//System.err.println("The element is not found");
		} catch (WebDriverException e) {
			reportSteps("fail", "Unknown Exception occured while finding element");
			//System.err.println("Unknown Exception occured while finding element");
		} 
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			reportSteps("fail", "The element is not found");
			//System.err.println("The element is not found");
		}catch (WebDriverException e) {
			// TODO Auto-generated catch block
			reportSteps("fail", "Unknown Exception occured while finding element using id");
			//System.err.println("Unknown Exception occured while finding element using id");
		}finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportSteps("pass", "The data "+data+" entered successfully");
		} catch (WebDriverException e) {
			reportSteps("fail", "The data "+data+" not entered successfully");
			//System.err.println("The data "+data+" not entered successfully");
		} finally {
			takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		// TODO Auto-generated method stub
		try {
			ele.clear();
			ele.sendKeys(data);
			reportSteps("pass", "The data "+data+" entered successfully");
			//System.out.println("The data "+data+" enter successfully");
		} catch (WebDriverException e) {
			reportSteps("fail", "The data "+data+" not entered successfully");
			//System.err.println("The data "+data+" not enter successfully");
		} finally {
			takeSnap();
		}	
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportSteps("pass", "The element "+ele+" clicked");
			//System.out.println("The element "+ele+" clicked");
		} catch (Exception e) {
			reportSteps("fail", "Unknown exception while doing click operation");
			//System.err.println("Unknown exception while doing click operation");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			//System.out.println("Text:" +text+ "Captured successfully");
			reportSteps("pass", "Text:" +text+ "Captured sucessfully");
			return text;
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			//System.out.println("Unknown exception occurred while getting text");
			reportSteps("fail", "Unknown exception occured while getting text");
		}finally {
			takeSnap();
		}		
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub

		try {
			Select dd= new Select(ele);
			dd.selectByVisibleText(value);
			//System.out.println("Selected " + value + " from drop-down");
			reportSteps("pass", "Selected " + value + " from drop-down");
		} catch (WebDriverException e) {
			reportSteps("fail", "Unknown Exception while selecting drop-down value using text");
			//System.err.println("Unknown Exception while selecting drop-down value using text");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select dd = new Select(ele);
			dd.selectByValue(value);			
			//String text = src.getText();
			//System.out.println("Selected " + value + " from drop-down");
			reportSteps("pass", "Selected " + value + " from drop-down");
		} catch (WebDriverException e) {
			//System.err.println("Unknown execption while selecting drop-down using value");
			reportSteps("fail", "Unknown execption while selecting drop-down using value");
		}finally {
			takeSnap();
		}		
	}


	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select dd = new Select(ele);
			dd.deselectByIndex(index);
			//System.out.println("Selected " + index + " from drop-down");
			reportSteps("pass", "Selected " + index + " from drop-down");
		} catch (WebDriverException e) {
			//System.err.println("Unknown execption while selecting drop-down using value");
			reportSteps("fail", "Unknown execption while selecting drop-down using value");
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		try {
			String title = driver.getTitle();
			boolean bReturn=false;
			if(title.equals(expectedTitle))
			{
				//System.out.println("Title matches with expected title");
				reportSteps("pass", "Title matches with expected title");
				bReturn=true;
			}else
				//System.err.println("Title not matches with expected title");
				reportSteps("fail", "Title not matches with expected title");
			return bReturn;
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			reportSteps("fail", "Unknown Error Occured while matching title with expected title");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(text.equals(expectedText))
			{
				//System.out.println("Text in field matches with expected text");
				reportSteps("pass", "Text in field matches with expected text");
			}
			else
				//System.err.println("Text in field not matching with expected text");
				reportSteps("fail", "Text in field not matching with expected text");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			//System.err.println("Unknown exception occured while verifying exact text");
			reportSteps("fail", "Unknown exception occured while verifying exact text");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(text.contains(expectedText))
			{
				//System.out.println("Text in field matches with expected text");
				reportSteps("pass", "Text in field matches with expected text");
			}
			else
				//System.err.println("Text in field not matching with expected text");
				reportSteps("fail", "Text in field not matching with expected text");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			//System.err.println("Unknown exception occured while verifying partial text");
			reportSteps("fail", "Unknown exception occured while verifying partial text");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> eachWindow = new ArrayList<>();
			eachWindow.addAll(allWindows);
			/*for (String string : eachWindow) {
				System.out.println(string);
			}*/
			driver.switchTo().window(eachWindow.get(index));
			//System.out.println("Switched to "+ index +" window successfully");
			reportSteps("pass", "Switched to "+ index +" window successfully");
		} catch (NoSuchWindowException e) {
			// TODO: handle exception
			//System.out.println("Window you are trying to access is not present");
			reportSteps("fail", "Window you are trying to access is not present");
		}
		catch (WebDriverException e) {
			//System.err.println("Unknown error while switching to another window");
			reportSteps("fail", "Unknown error while switching to another window");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().accept();
			//System.out.println("Alert Accepted successfully");
			reportSteps("pass", "Alert Accepted successfully");
		} catch (NoAlertPresentException e) {
			//System.err.println("Alert not present");
			reportSteps("fail", "Alert not present");
		}catch (WebDriverException e){
			//System.err.println("Unknown exception occured while accepting the alert");
			reportSteps("fail", "Unknown exception occured while accepting the alert");
		}
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
			//System.out.println("Alert dismissed successfully");
			reportSteps("pass", "Alert dismissed successfully");
		} catch (NoAlertPresentException e) {
			//System.err.println("Alert not present");
			reportSteps("fail", "Alert not present");
		}catch (WebDriverException e){
			//System.err.println("Unknown exception occured while dismissing the alert");
			reportSteps("fail", "Unknown exception occured while dismissing the alert");
		}
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().getText();
			//System.out.println("Alert text captured successfully");
			reportSteps("pass", "Alert text captured successfully");
		} catch (NoAlertPresentException e) {
			//System.err.println("Alert not present");
			reportSteps("fail", "Alert not present");
		}catch (WebDriverException e){
			//System.err.println("Unknown exception occured while capturing the text from alert");
			reportSteps("fail", "Unknown exception occured while capturing the text from alert");
		}
		return null;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			System.err.println("Error occured while taking the Screenshot");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		try {
			driver.close();
			//System.out.println("Browser closed successfully");
			reportSteps("pass", "Browser closed successfully");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			//System.err.println("Unknown exception while closing the browser");
			reportSteps("fail", "Unknown exception while closing the browser");
		}
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		try {
			driver.quit();
			//System.out.println("All Browsers closed successfully");
			reportSteps("pass", "All Browsers closed successfully");
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			//System.err.println("Unknown exception while closing all browsers");
			reportSteps("fail", "Unknown exception while closing all browsers");
		}

	}

	@Override
	public String getParentWindow() {
		// TODO Auto-generated method stub
		try {
			return driver.getWindowHandle();
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			reportSteps("fail", "Error occured while capturing the parent window handle");
		}
		return null;
	}

	@Override
	public void switchToParentWindow(String parentWindow) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().window(parentWindow);
			//System.out.println("Switched to parent window successfully");
			reportSteps("pass", "Switched to parent window successfully");
		} catch (WebDriverException e) {
			//System.out.println("Unknown exception occured while switching to parent window");
			reportSteps("fail", "Unknown exception occured while switching to parent window");
		}
	}

	@Override
	public void clickWithoutNoSnap(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			ele.click();
			//System.out.println("The element "+ele+" clicked");
			reportSteps("pass", "The element "+ele+" clicked");
		} catch (Exception e) {
			reportSteps("fail", "Unknown exception while doing click operation");
			//System.err.println("Unknown exception while doing click operation");
		}
	}

}
